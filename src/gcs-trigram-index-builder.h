/* gcs-trigram-index-builder.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "gcs-types.h"

G_BEGIN_DECLS

struct _GcsTrigramIndexBuilder
{
  GHashTable *trigrams;
  GPtrArray  *tombstones;
  GArray     *files;
  gint        in_bulk_insert;
};

void     gcs_trigram_index_builder_init              (GcsTrigramIndexBuilder      *self);
gsize    gcs_trigram_index_builder_add_file          (GcsTrigramIndexBuilder      *self,
                                                      const gchar                 *filename,
                                                      gint64                       mtime);
void     gcs_trigram_index_builder_add_tombstone     (GcsTrigramIndexBuilder      *self,
                                                      const gchar                 *filename);
void     gcs_trigram_index_builder_add_trigram       (GcsTrigramIndexBuilder      *self,
                                                      gsize                        file_id,
                                                      const GcsTrigram            *trigram,
                                                      goffset                      position);
void     gcs_trigram_index_builder_begin_bulk_insert (GcsTrigramIndexBuilder      *self);
void     gcs_trigram_index_builder_end_bulk_insert   (GcsTrigramIndexBuilder      *self);
gboolean gcs_trigram_index_builder_write             (GcsTrigramIndexBuilder      *self,
                                                      const gchar                 *filename,
                                                      GError                     **error);
void     gcs_trigram_index_builder_destroy           (GcsTrigramIndexBuilder      *self);
void     gcs_trigram_index_builder_foreach           (GcsTrigramIndexBuilder      *self,
                                                      GcsTrigramPositionCallback   callback,
                                                      gpointer                     callback_data);

G_END_DECLS
