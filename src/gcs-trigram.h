/* gcs-trigram.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <string.h>

#include "gcs-types.h"

G_BEGIN_DECLS

#define GCS_SPACE 0x20

struct _GcsTrigram
{
  gunichar a;
  gunichar b;
  gunichar c;
};

struct _GcsTrigramPosition
{
  gsize document_id;
  guint offset;
};

struct _GcsTrigramUtf8
{
  gchar  str[6*3+1];
  guint8 len;
};

struct _GcsTrigramStream
{
  GcsTrigramCallback callback;
  gpointer           callback_data;
  GcsTrigram         current;
};

gboolean gcs_trigram_to_utf8     (const GcsTrigram   *self,
                                  GcsTrigramUtf8     *utf8);
void     gcs_trigram_stream_init (GcsTrigramStream   *stream,
                                  GcsTrigramCallback  callback,
                                  gpointer            user_data);
void     gcs_trigram_stream_push (GcsTrigramStream   *stream,
                                  const gchar        *utf8,
                                  gsize               len);

static inline gboolean
gcs_trigram_is_valid (const GcsTrigram *trigram)
{
  return trigram->a && trigram->b && trigram->c;
}

static inline gboolean
gcs_trigram_push (GcsTrigram *trigram,
                  gunichar    ch)
{
  /* Ignore space after space */
  if (ch == GCS_SPACE && trigram->c == GCS_SPACE)
    return FALSE;

  trigram->a = trigram->b;
  trigram->b = trigram->c;
  trigram->c = ch;

  return TRUE;
}

static inline gboolean
gcs_trigram_empty (const GcsTrigram *trigram)
{
  return trigram->a == 0;
}

static inline gint
gcs_trigram_compare (const GcsTrigram *a,
                     const GcsTrigram *b)
{
  return memcmp (a, b, sizeof *a);
}

static inline gboolean
gcs_trigram_equal (const GcsTrigram *a,
                   const GcsTrigram *b)
{
  return gcs_trigram_compare (a, b) == 0;
}

static inline guint
gcs_trigram_hash (const GcsTrigram *trigram)
{
  return ((trigram->a & 0xFF) << 24) |
         ((trigram->b & 0xFF) << 16) |
         ((trigram->c & 0xFF) << 8) |
         (trigram->a | trigram->b | trigram->c) >> 8;
}

static inline gint
gcs_trigram_position_compare (const GcsTrigramPosition *a,
                              const GcsTrigramPosition *b)
{
  if (a->document_id < b->document_id)
    return -1;
  else if (a->document_id > b->document_id)
    return 1;
  else if (a->offset < b->offset)
    return -1;
  else if (a->offset > b->offset)
    return 1;
  else
    return 0;
}

G_END_DECLS
