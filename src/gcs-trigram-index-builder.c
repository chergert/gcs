/* gcs-trigram-index-builder.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gcs-trigram-index-builder"

#include "config.h"

#include <string.h>

#include "gcs-array.h"
#include "gcs-trigram.h"
#include "gcs-trigram-index-builder.h"

GCS_DEFINE_ARRAY (GcsTrigramPosition, gcs_trigram_position_array)

typedef struct
{
  gchar  *filename;
  gint64  mtime;
} File;

static void
clear_file (gpointer data)
{
  File *f = data;
  g_free (f->filename);
}

static gint
compare_string (gconstpointer a,
                gconstpointer b)
{
  const gchar * const *astr = a;
  const gchar * const *bstr = a;

  return g_strcmp0 (*astr, *bstr);
}

static void
gcs_trigram_index_builder_sort (GcsTrigramIndexBuilder *self)
{
  GHashTableIter iter;
  GcsTrigramPositionArray *ar;

  g_assert (self != NULL);
  g_assert (self->trigrams != NULL);
  g_assert (self->tombstones != NULL);

  g_hash_table_iter_init (&iter, self->trigrams);
  while (g_hash_table_iter_next (&iter, NULL, (gpointer *)&ar))
    gcs_trigram_position_array_sort (ar, (GCompareFunc)gcs_trigram_position_compare);

  g_ptr_array_sort (self->tombstones, compare_string);
}

void
gcs_trigram_index_builder_init (GcsTrigramIndexBuilder *self)
{
  g_assert (self != NULL);

  memset (self, 0, sizeof *self);

  self->trigrams = g_hash_table_new_full ((GHashFunc) gcs_trigram_hash,
                                          (GEqualFunc) gcs_trigram_equal,
                                          g_free,
                                          (GDestroyNotify) gcs_trigram_position_array_destroy);
  self->tombstones = g_ptr_array_new_with_free_func (g_free);
  self->files = g_array_new (FALSE, FALSE, sizeof (File));
  g_array_set_clear_func (self->files, clear_file);
}

gsize
gcs_trigram_index_builder_add_file (GcsTrigramIndexBuilder *self,
                                    const gchar            *filename,
                                    gint64                  mtime)
{
  File f;

  g_assert (self != NULL);

  f.filename = g_strdup (filename);
  f.mtime = mtime;

  g_array_append_val (self->files, f);

  return self->files->len;
}

void
gcs_trigram_index_builder_add_tombstone (GcsTrigramIndexBuilder *self,
                                         const gchar            *filename)
{
  g_assert (self != NULL);
  g_assert (filename != NULL);

  g_ptr_array_add (self->tombstones, g_strdup (filename));

  if G_UNLIKELY (self->in_bulk_insert == 0)
    g_ptr_array_sort (self->tombstones, compare_string);
}

void
gcs_trigram_index_builder_add_trigram (GcsTrigramIndexBuilder *self,
                                       gsize                   file_id,
                                       const GcsTrigram       *trigram,
                                       goffset                 position)
{
  GcsTrigramPositionArray *ar;
  GcsTrigramPosition pos;

  g_assert (self != NULL);
  g_assert (file_id > 0);
  g_assert (trigram != NULL);

  pos.document_id = file_id;
  pos.offset = position;

  if (!(ar = g_hash_table_lookup (self->trigrams, trigram)))
    {
      ar = gcs_trigram_position_array_new (0, NULL);
      g_hash_table_insert (self->trigrams,
                           g_memdup (trigram, sizeof *trigram),
                           ar);
    }

  gcs_trigram_position_array_append (ar, &pos);

  if G_UNLIKELY (self->in_bulk_insert == 0)
    gcs_trigram_position_array_sort (ar, (GCompareFunc)gcs_trigram_position_compare);
}

gboolean
gcs_trigram_index_builder_write (GcsTrigramIndexBuilder  *self,
                                 const gchar             *filename,
                                 GError                 **error)
{
  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (self->in_bulk_insert == 0, FALSE);
  g_return_val_if_fail (filename != NULL, FALSE);

  return TRUE;
}

void
gcs_trigram_index_builder_destroy (GcsTrigramIndexBuilder *self)
{
  g_assert (self != NULL);

  g_clear_pointer (&self->trigrams, g_hash_table_unref);
  g_clear_pointer (&self->tombstones, g_ptr_array_unref);
  g_clear_pointer (&self->files, g_array_unref);
}

void
gcs_trigram_index_builder_begin_bulk_insert (GcsTrigramIndexBuilder *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->in_bulk_insert >= 0);

  self->in_bulk_insert++;
}

void
gcs_trigram_index_builder_end_bulk_insert (GcsTrigramIndexBuilder *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->in_bulk_insert >= 0);

  self->in_bulk_insert--;

  if (self->in_bulk_insert == 0)
    gcs_trigram_index_builder_sort (self);
}

void
gcs_trigram_index_builder_foreach (GcsTrigramIndexBuilder     *self,
                                   GcsTrigramPositionCallback  callback,
                                   gpointer                    callback_data)
{
  const GcsTrigramPositionArray *ar;
  const GcsTrigram *trigram;
  GHashTableIter iter;

  g_return_if_fail (self != NULL);
  g_return_if_fail (callback != NULL);

  g_hash_table_iter_init (&iter, self->trigrams);

  while (g_hash_table_iter_next (&iter, (gpointer *)&trigram, (gpointer *)&ar))
    callback (trigram, ar->items, ar->len, callback_data);
}
