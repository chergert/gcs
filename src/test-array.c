#include "gcs-array.h"

GCS_DEFINE_ARRAY (gint, gint_array)

static gint
cmpint (gconstpointer a,
        gconstpointer b)
{
  return *(gint *)a - *(gint *)b;
}

static void
test_it (void)
{
  gintArray ar;
  gint v = 32;

  gint_array_init (&ar, 12, NULL);
  g_assert_cmpint (0, ==, gint_array_append (&ar, &v));
  v = 2;
  g_assert_cmpint (1, ==, gint_array_append (&ar, &v));
  g_assert_cmpint (ar.len, ==, 2);
  g_assert_cmpint (32, ==, ar.items[0]);
  g_assert_cmpint (2, ==, ar.items[1]);
  gint_array_sort (&ar, cmpint);
  g_assert_cmpint (ar.len, ==, 2);
  g_assert_cmpint (2, ==, ar.items[0]);
  g_assert_cmpint (32, ==, ar.items[1]);
  gint_array_remove (&ar, 0);
  g_assert_cmpint (32, ==, ar.items[0]);
  g_assert_cmpint (1, ==, ar.len);
  gint_array_remove (&ar, 0);
  g_assert_cmpint (0, ==, ar.len);
  v = 1;
  g_assert_cmpint (0, ==, gint_array_append (&ar, &v));
  v = 2;
  g_assert_cmpint (1, ==, gint_array_append (&ar, &v));
  v = 3;
  g_assert_cmpint (2, ==, gint_array_append (&ar, &v));
  g_assert_cmpint (ar.len, ==, 3);
  g_assert_cmpint (1, ==, ar.items[0]);
  g_assert_cmpint (2, ==, ar.items[1]);
  g_assert_cmpint (3, ==, ar.items[2]);
  gint_array_remove_fast (&ar, 0);
  g_assert_cmpint (3, ==, ar.items[0]);
  g_assert_cmpint (2, ==, ar.items[1]);
  g_assert_cmpint (ar.len, ==, 2);
  gint_array_destroy (&ar);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Gcs/Array/basic", test_it);
  return g_test_run ();
}
