#include "gcs-sparse-set.h"

#define PTR(v) GSIZE_TO_POINTER(v)

static void
count_items (gpointer data,
             gpointer user_data)
{
  gsize *count = user_data;
  g_assert (*count == GPOINTER_TO_SIZE (data));
  (*count)++;
}

static void
test_it (void)
{
  GcsSparseSet set;
  gsize count = 0;

  gcs_sparse_set_init (&set, 1024, NULL);

  for (gsize i = 0; i < 1024; i++)
    {
      g_assert_cmpint (i, ==, gcs_sparse_set_get_size (&set));
      g_assert_false (gcs_sparse_set_contains (&set, i));
      gcs_sparse_set_insert (&set, i, PTR(i));
      g_assert_true (gcs_sparse_set_contains (&set, i));
      g_assert_cmpint (i, ==, GPOINTER_TO_SIZE (gcs_sparse_set_lookup (&set, i)));
    }

  gcs_sparse_set_foreach (&set, count_items, &count);
  g_assert_cmpint (count, ==, 1024);

  for (gsize i = 0; i < 1024; i++)
    {
      g_assert_true (gcs_sparse_set_contains (&set, i));
      gcs_sparse_set_remove (&set, i);
      g_assert_false (gcs_sparse_set_contains (&set, i));
      g_assert_cmpint (1024 - i - 1, ==, gcs_sparse_set_get_size (&set));
    }

  g_assert_cmpint (0, ==, gcs_sparse_set_get_size (&set));

  count = 0;
  gcs_sparse_set_foreach (&set, count_items, &count);
  g_assert_cmpint (count, ==, 0);

  for (gsize i = 0; i < 1024; i++)
    {
      g_assert_cmpint (i, ==, gcs_sparse_set_get_size (&set));
      g_assert_false (gcs_sparse_set_contains (&set, i));
      gcs_sparse_set_insert (&set, i, PTR(i));
      g_assert_true (gcs_sparse_set_contains (&set, i));
      g_assert_cmpint (i, ==, GPOINTER_TO_SIZE (gcs_sparse_set_lookup (&set, i)));
    }

  gcs_sparse_set_reset (&set);
  g_assert_cmpint (0, ==, gcs_sparse_set_get_size (&set));

  count = 0;
  gcs_sparse_set_foreach (&set, count_items, &count);
  g_assert_cmpint (count, ==, 0);

  gcs_sparse_set_destroy (&set);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Gcs/SparseSet/basic", test_it);
  return g_test_run ();
}
