/* gcs-types.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef struct _GcsArray               GcsArray;
typedef struct _GcsSparseSet           GcsSparseSet;
typedef struct _GcsSparseItem          GcsSparseItem;
typedef struct _GcsDenseItem           GcsDenseItem;
typedef struct _GcsQuery               GcsQuery;
typedef struct _GcsQueryNode           GcsQueryNode;
typedef struct _GcsTrigram             GcsTrigram;
typedef struct _GcsTrigramIndexBuilder GcsTrigramIndexBuilder;
typedef struct _GcsTrigramPosition     GcsTrigramPosition;
typedef struct _GcsTrigramStream       GcsTrigramStream;
typedef struct _GcsTrigramTree         GcsTrigramTree;
typedef struct _GcsTrigramTreeNode     GcsTrigramTreeNode;
typedef struct _GcsTrigramUtf8         GcsTrigramUtf8;

typedef void (*GcsTrigramCallback)         (const GcsTrigram         *trigram,
                                            goffset                   position,
                                            gpointer                  user_data);
typedef void (*GcsTrigramPositionCallback) (const GcsTrigram         *trigram,
                                            const GcsTrigramPosition *positions,
                                            gsize                     n_positions,
                                            gpointer                  user_data);

G_END_DECLS
