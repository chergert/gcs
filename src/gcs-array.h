/* gcs-array.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdlib.h>
#include <string.h>

#include "gcs-types.h"

G_BEGIN_DECLS

#define GCS_DEFINE_ARRAY(Type, _type)                                        \
  typedef struct _##Type##Array Type##Array;                                 \
  struct _##Type##Array {                                                    \
    gsize len;                                                               \
    gsize alloc_len;                                                         \
    Type *items;                                                             \
    GDestroyNotify destroy;                                                  \
    guint is_alloc : 1;                                                      \
  };                                                                         \
                                                                             \
  static inline void                                                         \
  _type##_init (Type##Array *ar, gsize size, GDestroyNotify destroy)         \
  {                                                                          \
    if (size < 16)                                                           \
      size = 16;                                                             \
    ar->len = 0;                                                             \
    ar->alloc_len = size;                                                    \
    ar->items = g_new (Type, size);                                          \
    ar->destroy = destroy;                                                   \
  }                                                                          \
                                                                             \
  static inline Type##Array *                                                \
  _type##_new (gsize size, GDestroyNotify destroy)                           \
  {                                                                          \
    Type##Array *ret = g_slice_new0 (Type##Array);                           \
    _type##_init (ret, size, destroy);                                       \
    ret->is_alloc = TRUE;                                                    \
    return ret;                                                              \
  }                                                                          \
                                                                             \
  static inline void                                                         \
  _type##_destroy (Type##Array *ar)                                          \
  {                                                                          \
    if (ar->destroy != NULL)                                                 \
      {                                                                      \
        for (gsize i = 0; i < ar->len; i++)                                  \
          ar->destroy (&ar->items[i]);                                       \
      }                                                                      \
                                                                             \
    g_free (ar->items);                                                      \
                                                                             \
    ar->len = 0;                                                             \
    ar->alloc_len = 0;                                                       \
    ar->items = NULL;                                                        \
    ar->destroy = NULL;                                                      \
                                                                             \
    if (ar->is_alloc)                                                        \
      g_slice_free (Type##Array, ar);                                        \
  }                                                                          \
                                                                             \
  static inline gsize                                                        \
  _type##_append (Type##Array *ar, const Type *val)                          \
  {                                                                          \
    gsize pos;                                                               \
                                                                             \
    if G_UNLIKELY (ar->len == ar->alloc_len)                                 \
      {                                                                      \
        if G_UNLIKELY (G_MAXSIZE - ar->alloc_len < ar->alloc_len)            \
          g_error ("Not enough addressible memory for operation");           \
        ar->items = g_renew (Type, ar->items, ar->alloc_len * 2);            \
        ar->alloc_len *= 2;                                                  \
      }                                                                      \
                                                                             \
    pos = ar->len++;                                                         \
    ar->items[pos] = *val;                                                   \
    return pos;                                                              \
  }                                                                          \
                                                                             \
  static inline void                                                         \
  _type##_sort (Type##Array *ar, GCompareFunc compare)                       \
  {                                                                          \
    if (ar->len > 0)                                                         \
      qsort (ar->items, ar->len, sizeof (Type), compare);                    \
  }                                                                          \
                                                                             \
  static inline gboolean                                                     \
  _type##_search (Type##Array *ar,                                           \
                  gconstpointer target,                                      \
                  GCompareFunc compare,                                      \
                  gsize *out_match_index)                                    \
  {                                                                          \
    Type *ret;                                                               \
                                                                             \
    if (ar->len == 0)                                                        \
      return FALSE;                                                          \
                                                                             \
    ret = bsearch (target, ar->items, ar->len, sizeof (Type), compare);      \
                                                                             \
    if (ret != NULL)                                                         \
      {                                                                      \
        if (out_match_index)                                                 \
          *out_match_index = ret - ar->items;                                \
        return TRUE;                                                         \
      }                                                                      \
                                                                             \
    return FALSE;                                                            \
  }                                                                          \
                                                                             \
  static inline void                                                         \
  _type##_remove (Type##Array *ar, gsize position)                           \
  {                                                                          \
    ar->len--;                                                               \
    if (position != ar->len)                                                 \
      memmove (&ar->items[position],                                         \
               &ar->items[position+1],                                       \
               sizeof (Type) * (ar->len - position));                        \
  }                                                                          \
                                                                             \
  static inline void                                                         \
  _type##_remove_fast (Type##Array *ar, gsize position)                      \
  {                                                                          \
    ar->len--;                                                               \
    if (position != ar->len)                                                 \
      ar->items[position] = ar->items[ar->len];                              \
  }

G_END_DECLS
