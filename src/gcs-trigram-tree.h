/* gcs-trigram-tree.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "iqueue.h"

#include "gcs-array.h"
#include "gcs-trigram.h"
#include "gcs-types.h"

G_BEGIN_DECLS

#define GCS_TRIGRAM_TREE_BRANCH_SIZE 125

GCS_DEFINE_ARRAY (GcsTrigramPosition, gcs_trigram_position)

struct _GcsTrigramTreeNode
{
  IQUEUE_NODE (guint8, GCS_TRIGRAM_TREE_BRANCH_SIZE) iqueue;
  guint8 kind : 1;
  guint8 unused : 7;
  GcsTrigram keys[GCS_TRIGRAM_TREE_BRANCH_SIZE];
  union {
    struct {
      GcsTrigramTreeNode *children[GCS_TRIGRAM_TREE_BRANCH_SIZE];
    } branch;
    struct {
      GcsTrigramPosition positions[GCS_TRIGRAM_TREE_BRANCH_SIZE];
    } leaf;
  } u;
} __attribute__((aligned(1)));

struct _GcsTrigramTree
{
  GcsTrigramTreeNode *root;
};

GcsTrigramTree          *gcs_trigram_tree_new  (void);
void                     gcs_trigram_tree_free (GcsTrigramTree   *self);
GcsTrigramPositionArray *gcs_trigram_tree_get  (GcsTrigramTree   *self,
                                                const GcsTrigram *trigram);

G_END_DECLS
