/* gcs-sparse-set.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "gcs-array.h"
#include "gcs-types.h"

G_BEGIN_DECLS

struct _GcsDenseItem
{
  gsize    sparse;
  gpointer data;
};

struct _GcsSparseItem
{
  gsize dense;
};

GCS_DEFINE_ARRAY (GcsSparseItem, gcs_sparse_item_array)
GCS_DEFINE_ARRAY (GcsDenseItem, gcs_dense_item_array)

struct _GcsSparseSet
{
  GcsDenseItemArray  dense;
  GcsSparseItemArray sparse;
  GDestroyNotify     destroy;
};

void     gcs_sparse_set_init     (GcsSparseSet             *set,
                                  gsize                     max_items,
                                  GDestroyNotify            destroy);
void     gcs_sparse_set_destroy  (GcsSparseSet             *set);
gsize    gcs_sparse_set_get_size (const GcsSparseSet      *set);
void     gcs_sparse_set_insert   (GcsSparseSet            *set,
                                  gsize                    key,
                                  gpointer                 value);
gboolean gcs_sparse_set_contains (const GcsSparseSet      *set,
                                  gsize                    key);
gpointer gcs_sparse_set_lookup   (const GcsSparseSet      *set,
                                  gsize                    key);
void     gcs_sparse_set_remove   (GcsSparseSet            *set,
                                  gsize                    key);
void     gcs_sparse_set_foreach  (const GcsSparseSet      *set,
                                  GFunc                    foreach_func,
                                  gpointer                 foreach_data);
void     gcs_sparse_set_reset    (GcsSparseSet            *set);

G_END_DECLS
