#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "gcs-query.h"
#include "gcs-trigram-index-builder.h"
#include "gcs-trigram-tree.h"
#include "gcs-trigram.h"
#include "gcs-types.h"

static gsize doc_id;

static void
trigram_cb (const GcsTrigram *trigram,
            goffset           position,
            gpointer          user_data)
{
  GcsTrigramIndexBuilder *builder = user_data;

  g_assert (trigram != NULL);
  g_assert (builder != NULL);

  gcs_trigram_index_builder_add_trigram (builder, doc_id, trigram, position);
}

static void
foreach_cb (const GcsTrigram         *trigram,
            const GcsTrigramPosition *positions,
            gsize                     n_positions,
            gpointer                  user_data)
{
  GcsTrigramUtf8 utf8;

  gcs_trigram_to_utf8 (trigram, &utf8);
  g_print ("%04"G_GSIZE_MODIFIER"d: %s\n", n_positions, utf8.str);
}

static gchar *
parse_query (gint    *argc,
             gchar ***argv)
{
  g_autoptr(GOptionContext) context = NULL;
  g_autofree gchar *query = NULL;
  const GOptionEntry entries[] = {
    { "query", 'q', 0, G_OPTION_ARG_STRING, &query, "A query to search for" },
  };

  context = g_option_context_new ("- GNOME code search");
  g_option_context_add_main_entries (context, entries, NULL);

  if (g_option_context_parse (context, argc, argv, NULL))
    return g_steal_pointer (&query);

  return NULL;
}

gint
main (gint   argc,
      gchar *argv[])
{
  GcsTrigramIndexBuilder builder;
  g_autofree gchar *query = NULL;

  query = parse_query (&argc, &argv);

  gcs_trigram_index_builder_init (&builder);
  gcs_trigram_index_builder_begin_bulk_insert (&builder);

  for (guint i = 1; i < argc; i++)
    {
      g_autofree gchar *contents = NULL;
      GcsTrigramStream stream;
      struct stat stbuf;
      gsize len = 0;

      if (stat (argv[i], &stbuf) != 0)
        {
          g_printerr ("Failed to get file info: %s\n", argv[i]);
          continue;
        }

      if (!g_file_test (argv[i], G_FILE_TEST_EXISTS) ||
          !g_file_get_contents (argv[i], &contents, &len, NULL))
        {
          g_printerr ("Failed to locate file %s\n", argv[i]);
          continue;
        }

      doc_id = gcs_trigram_index_builder_add_file (&builder, argv[i], stbuf.st_mtime);

      gcs_trigram_stream_init (&stream, trigram_cb, &builder);
      gcs_trigram_stream_push (&stream, contents, len);
    }

  gcs_trigram_index_builder_end_bulk_insert (&builder);

  if (query == NULL)
    {
      gcs_trigram_index_builder_foreach (&builder, foreach_cb, NULL);
    }
  else
    {
      GcsQuery q;

      gcs_query_init (&q, query);

      gcs_query_node_printf (q.ast, 0);

      gcs_query_destroy (&q);
    }

  return 0;
}
