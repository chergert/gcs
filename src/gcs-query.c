/* gcs-query.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gcs-query"

#include "config.h"

#include "gcs-query.h"
#include "gcs-trigram.h"

enum {
  KIND_NOP,
  KIND_IN,
  KIND_NIN,
  KIND_AND,
};

static void
gcs_query_node_destroy (GcsQueryNode *node)
{
  g_assert (node != NULL);

  if (node->kind == KIND_AND)
    {
      gcs_query_node_destroy (node->u.and.left);
      gcs_query_node_destroy (node->u.and.right);
    }

  g_slice_free (GcsQueryNode, node);
}

static GcsQueryNode *
gcs_query_node_new (gint kind)
{
  GcsQueryNode *ret;

  ret = g_slice_new0 (GcsQueryNode);
  ret->kind = kind;

  return ret;
}

static GcsQueryNode *
gcs_query_node_new_and (GcsQueryNode *left,
                        GcsQueryNode *right)
{
  GcsQueryNode *ret;

  g_assert (left != NULL);
  g_assert (right != NULL);

  ret = g_slice_new0 (GcsQueryNode);
  ret->kind = KIND_AND;
  ret->u.and.left = left;
  ret->u.and.right = right;

  return ret;
}

static void
trigram_cb (const GcsTrigram *trigram,
            goffset           position,
            gpointer          user_data)
{
  GcsQuery *self = user_data;
  GcsQueryNode *node;

  g_assert (self != NULL);
  g_assert (trigram != NULL);

  /* TODO: We need to do actual regex parsing rather
   *       that simple "in" sets.
   */

  node = gcs_query_node_new (KIND_IN);
  node->u.in.trigram = *trigram;

  self->ast = gcs_query_node_new_and (self->ast, node);
}

void
gcs_query_init (GcsQuery    *self,
                const gchar *str)
{
  GcsTrigramStream stream;

  g_return_if_fail (self != NULL);
  g_return_if_fail (str != NULL);

  memset (self, 0, sizeof *self);

  self->ast = gcs_query_node_new (KIND_NOP);

  gcs_trigram_stream_init (&stream, trigram_cb, self);
  gcs_trigram_stream_push (&stream, str, strlen (str));
}

void
gcs_query_destroy (GcsQuery *self)
{
  g_return_if_fail (self != NULL);

  g_clear_pointer (&self->ast, gcs_query_node_destroy);
}

static void
space (guint depth)
{
  for (guint i = 0; i < depth; i++)
    g_printerr ("  ");
}

void
gcs_query_node_printf (GcsQueryNode *node,
                       guint         depth)
{
  space (depth);

  if (node == NULL)
    {
      g_printerr ("GcsQueryNode(NULL)\n");
    }
  else if (node->kind == KIND_NOP)
    {
      g_printerr ("GcsQueryNode(NOP)\n");
    }
  else if (node->kind == KIND_IN)
    {
      GcsTrigramUtf8 utf8;

      gcs_trigram_to_utf8 (&node->u.in.trigram, &utf8);
      g_printerr ("GcsQueryNode(IN: %s)\n", utf8.str);
    }
  else if (node->kind == KIND_NIN)
    {
      GcsTrigramUtf8 utf8;

      gcs_trigram_to_utf8 (&node->u.in.trigram, &utf8);
      g_printerr ("GcsQueryNode(NIN: %s)\n", utf8.str);
    }
  else if (node->kind == KIND_AND)
    {
      g_printerr ("GcsQueryNode(AND) {\n");
      gcs_query_node_printf (node->u.and.left, depth + 1);
      gcs_query_node_printf (node->u.and.right, depth + 1);
      space (depth);
      g_printerr ("}\n");
    }
  else
    {
      g_printerr ("GcsQueryNode(INVALID)\n");
    }
}
