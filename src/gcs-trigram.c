/* gcs-trigram.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gcs-trigram"

#include "config.h"

#include <string.h>

#include "gcs-trigram.h"

void
gcs_trigram_stream_init (GcsTrigramStream   *stream,
                         GcsTrigramCallback  callback,
                         gpointer            user_data)
{
  g_assert (stream != NULL);
  g_assert (callback != NULL);

  memset (stream, 0, sizeof *stream);

  stream->callback = callback;
  stream->callback_data = user_data;
}

void
gcs_trigram_stream_push (GcsTrigramStream *stream,
                         const gchar      *utf8,
                         gsize             len)
{
  GcsTrigramCallback callback;
  const gchar *begin = utf8;
  const gchar *endptr;
  gpointer callback_data;
  gsize count = 0;

  g_assert (stream != NULL);
  g_assert (utf8 != NULL);

  if (len == 0)
    return;

  if (len < 0)
    len = strlen (utf8);

  callback = stream->callback;
  callback_data = stream->callback_data;
  endptr = utf8 + len;

  while (utf8 < endptr)
    {
      gunichar ch;

      g_assert (utf8 < endptr);

      ch = g_utf8_get_char_validated (utf8, endptr - utf8);

      if (ch == -1)
        {
          /* Invalid UTF-8 */
          g_return_if_reached ();
        }
      else if (ch == -2)
        {
          /* Not enough data to extract the character. We might
           * want to support incrementally pushing data eventually,
           * and if so, this is where we would want to special case
           * that (along with handling the overflow on next read).
           */
          g_return_if_reached ();
        }
      else
        {
          /* Use non-breaking space to represent all space */
          if (g_unichar_isspace (ch))
            ch = GCS_SPACE;

          if (gcs_trigram_push (&stream->current, ch))
            {
              count++;
              if (count >= 3)
                callback (&stream->current, utf8 - begin, callback_data);
            }
        }

      utf8 = g_utf8_next_char (utf8);
    }
}

gboolean
gcs_trigram_to_utf8 (const GcsTrigram *self,
                     GcsTrigramUtf8   *utf8)
{
  gchar *ptr;
  gint len;

  g_assert (self != NULL);
  g_assert (utf8 != NULL);

  ptr = &utf8->str[0];
  len = g_unichar_to_utf8 (self->a, ptr);
  ptr += len;
  len = g_unichar_to_utf8 (self->b, ptr);
  ptr += len;
  len = g_unichar_to_utf8 (self->c, ptr);
  ptr += len;
  *ptr = 0;

  utf8->len = ptr - utf8->str;

  return TRUE;
}
