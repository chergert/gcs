/* gcs-sparse-set.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gcs-sparse-set"

#include "config.h"

#include "gcs-sparse-set.h"

void
gcs_sparse_set_init (GcsSparseSet   *set,
                     gsize           max_items,
                     GDestroyNotify  destroy)
{
  g_assert (set != NULL);
  g_assert (max_items > 0);

  gcs_dense_item_array_init (&set->dense, 0, NULL);
  gcs_sparse_item_array_init (&set->sparse, max_items, NULL);
  set->sparse.len = set->sparse.alloc_len;
  set->destroy = destroy;

  g_assert (set->sparse.len >= max_items);
}

void
gcs_sparse_set_destroy (GcsSparseSet *set)
{
  g_assert (set != NULL);

  if (set->destroy != NULL)
    {
      for (gsize i = 0; i < set->dense.len; i++)
        set->destroy (set->dense.items[i].data);
    }

  gcs_dense_item_array_destroy (&set->dense);
  gcs_sparse_item_array_destroy (&set->sparse);
  set->destroy = NULL;
}

gsize
gcs_sparse_set_get_size (const GcsSparseSet *set)
{
  g_assert (set != NULL);

  return set->dense.len;
}

gboolean
gcs_sparse_set_contains (const GcsSparseSet *set,
                         gsize               key)
{
  gsize dense;

  g_assert (set != NULL);
  g_assert (key < set->sparse.len);

  dense = set->sparse.items[key].dense;

  if (dense < set->dense.len)
    {
      const GcsDenseItem *item = &set->dense.items[dense];
      return item->sparse == key;
    }

  return FALSE;
}

static GcsDenseItem *
gcs_sparse_set_lookup_item (const GcsSparseSet *set,
                            gsize               key)
{
  gsize dense;

  g_assert (set != NULL);
  g_assert (key < set->sparse.len);

  dense = set->sparse.items[key].dense;

  if (dense < set->dense.len)
    {
      GcsDenseItem *item = &set->dense.items[dense];

      if (item->sparse == key)
        return item;
    }

  return NULL;
}

void
gcs_sparse_set_insert (GcsSparseSet *set,
                       gsize         key,
                       gpointer      value)
{
  GcsDenseItem *itemptr;

  g_assert (set != NULL);
  g_assert (key < set->sparse.len);

  if G_UNLIKELY ((itemptr = gcs_sparse_set_lookup_item (set, key)))
    {
      if (value != itemptr->data)
        {
          if (set->destroy != NULL)
            set->destroy (itemptr->data);
          itemptr->data = value;
        }
    }
  else
    {
      GcsDenseItem item;

      item.sparse = key;
      item.data = value;
      set->sparse.items[key].dense = gcs_dense_item_array_append (&set->dense, &item);
    }
}

void
gcs_sparse_set_remove (GcsSparseSet *set,
                       gsize         key)
{
  gsize dense;

  g_assert (set != NULL);
  g_assert (key < set->sparse.len);

  dense = set->sparse.items[key].dense;

  if (dense < set->dense.len && set->dense.items[dense].sparse == key)
    {
      gcs_dense_item_array_remove_fast (&set->dense, dense);

      /* Since we moved the tail item with remove_fast(), we need to update the
       * backpointer from sparse to point at the new position (but only if we
       * weren't at the end of the dense array, in which case len-- protects
       * us from invalid use).
       */
      if (dense < set->dense.len)
        set->sparse.items[set->dense.items[dense].sparse].dense = dense;
    }
}

gpointer
gcs_sparse_set_lookup (const GcsSparseSet *set,
                       gsize               key)
{
  gsize dense;

  g_assert (set != NULL);
  g_assert (key < set->sparse.len);

  dense = set->sparse.items[key].dense;

  if (dense < set->dense.len)
    {
      const GcsDenseItem *item = &set->dense.items[dense];

      if (item->sparse == key)
        return item->data;
    }

  return NULL;
}

void
gcs_sparse_set_foreach (const GcsSparseSet *set,
                        GFunc               foreach_func,
                        gpointer            foreach_data)
{
  g_assert (set != NULL);
  g_assert (foreach_func != NULL);

  for (gsize i = 0; i < set->dense.len; i++)
    foreach_func (set->dense.items[i].data, foreach_data);
}

void
gcs_sparse_set_reset (GcsSparseSet *set)
{
  g_assert (set != NULL);

  set->dense.len = 0;
}
