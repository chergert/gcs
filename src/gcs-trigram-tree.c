/* gcs-trigram-tree.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "gcs-trigram-tree.h"

enum {
  KIND_BRANCH = 0,
  KIND_LEAF   = 1,
};

static gboolean    gcs_trigram_tree_node_full          (const GcsTrigramTreeNode *node);
static gboolean    gcs_trigram_tree_node_search        (GcsTrigramTreeNode       *node,
                                                        const GcsTrigram         *trigram,
                                                        guint                    *index);
static gboolean    gcs_trigram_tree_node_search_branch (GcsTrigramTreeNode       *node,
                                                        const GcsTrigram         *trigram,
                                                        guint                    *index);
static gboolean    gcs_trigram_tree_node_search_leaf   (GcsTrigramTreeNode       *node,
                                                        const GcsTrigram         *trigram,
                                                        guint                    *index);

static gboolean
gcs_trigram_tree_node_full (const GcsTrigramTreeNode *node)
{
  return node->iqueue.length == (GCS_TRIGRAM_TREE_BRANCH_SIZE - 1);
}

/**
 * SECTION:gcs-trigram-tree
 * @title: GcsTrigramTree
 * @short_description: a tree to build trigram indexes
 *
 * The #GcsTrigramTree is used to build the index of trigrams. It is
 * essentially a n-ary btree that only allows adding new items to the tree.
 * The fanout allows us to avoid pointer chaising like we would with a RBTree
 * or other binary-based tree.
 *
 * The data values with in the nodes point to an array that we can use to
 * store the document/position information.
 */

static GcsTrigramTreeNode *
gcs_trigram_tree_node_new (guint kind)
{
  GcsTrigramTreeNode *node;

  node = g_slice_new0 (GcsTrigramTreeNode);
  node->kind = kind;
  IQUEUE_INIT (&node->iqueue);

  return g_steal_pointer (&node);
}

static gboolean
gcs_trigram_tree_node_search_leaf (GcsTrigramTreeNode *node,
                                   const GcsTrigram   *trigram,
                                   guint              *index)
{
  g_assert (node != NULL);
  g_assert (node->kind == KIND_LEAF);
  g_assert (trigram != NULL);
  g_assert (index != NULL);

  IQUEUE_FOREACH (&node->iqueue, iq, {
    gint cmpval = gcs_trigram_compare (trigram, &node->keys[iq]);

    if (cmpval == 0)
      {
        *index = iq;
        return TRUE;
      }

    if (cmpval > 0)
      return FALSE;
  });

  return FALSE;
}

static gboolean
gcs_trigram_tree_node_search_branch (GcsTrigramTreeNode *node,
                                     const GcsTrigram   *trigram,
                                     guint              *index)
{
  guint i = 0;

  g_assert (node != NULL);
  g_assert (node->kind == KIND_BRANCH);
  g_assert (trigram != NULL);
  g_assert (index != NULL);

  IQUEUE_FOREACH (&node->iqueue, iq, {
    if (gcs_trigram_compare (trigram, &node->keys[iq]) > 0)
      break;
    i = iq;
  });

  return gcs_trigram_tree_node_search (node->u.branch.children[i], trigram, index);
}

static gboolean
gcs_trigram_tree_node_search (GcsTrigramTreeNode *node,
                              const GcsTrigram   *trigram,
                              guint              *index)
{
  g_assert (node != NULL);
  g_assert (trigram != NULL);
  g_assert (index != NULL);

  if (node->kind == KIND_LEAF)
    return gcs_trigram_tree_node_search_leaf (node, trigram, index);
  else
    return gcs_trigram_tree_node_search_branch (node, trigram, index);
}

GcsTrigramTree *
gcs_trigram_tree_new (void)
{
  GcsTrigramTree *self;

  self = g_slice_new0 (GcsTrigramTree);

  return self;
}

void
gcs_trigram_tree_free (GcsTrigramTree *self)
{
  g_slice_free (GcsTrigramTree, self);
}

static void
gcs_trigram_tree_insert (GcsTrigramTree           *self,
                         const GcsTrigram         *trigram,
                         const GcsTrigramPosition *position)
{
  g_assert (self != NULL);
  g_assert (trigram != NULL);

  if (self->root == NULL)
    {
      self->root = gcs_trigram_tree_node_new (KIND_LEAF);
      self->root->keys[0] = *trigram;
      self->root->u.leaf.positions[0] = *position;
      IQUEUE_INSERT (&self->root->iqueue, 0, 0);
      return;
    }

  if (gcs_trigram_tree_node_full (self->root))
    {
      GcsTrigramTreeNode *new_root = gcs_trigram_tree_node_new (KIND_BRANCH);

      new_root->u.branch.children[0] = self->root;
    }


}

GcsTrigramPositionArray *
gcs_trigram_tree_get (GcsTrigramTree   *self,
                      const GcsTrigram *trigram)
{
  guint index;

  g_assert (self != NULL);
  g_assert (trigram != NULL);

  if (gcs_trigram_tree_node_search (self->root, trigram, &index))
    {
    }

  return NULL;
}
